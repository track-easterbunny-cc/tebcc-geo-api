# alembic.ini/.env combined docs
In `alembic.ini` and `.env` there are two connection strings to worry about when attempting to set up your own DB. Hopefully this document can describe a little bit of how to get it working properly.

# Where to look for the connection string
Line 62 of `alembic.ini`, with the key `sqlalchemy.url`. 

In `.env`, it's the only option there.

# How to format your connection string
## For a connection to a MySQL-compatible database...

`mysql+pymysql://username:password@dburl:dbport/dbname`

Fill in:
* `username` with the username that has privileges on the database specified in `dbname`
* `password` with the password of the username that has privileges on the database specified in `dbname`
* `dburl` with the FQDN of the database you're connecting to
* `dbport` with the port number that the database is on. Usually if the database is on the same machine that the Geo API is running on, the combination of `dburl` and `dbport` is `localhost:3306`
* `dbname` is the name of the database that tables will be inserted into.

## For a SQLite database...

`sqlite:///dbfile.db`

Fill in:
* `dbfile` where the file is the name of your database. This should be in the same directory as the Geo API - or wherever your relative working directory is.

## For anything else...

Google is your friend. You will also need to regenerate the alembic files as described in the main readme.