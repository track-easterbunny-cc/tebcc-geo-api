# TEBCC Twitter Bot CI Pipeline Documentation
This document describes how the GitLab CI pipelines work for the Twitter Bot.

It's best you get familiar with the documentation of the main project's CI pipelines before reading this documentation.

# Stages
* Deploy

# Deploy
In short, the deploy stage just mirrors all the files in the Geo API directory to the remote server to deploy the Geo API.

# GitLab CI variables
These are the variables you need to configure to make the pipelines run successfully.

When making these variables in GitLab settings, you can use the default values (unless otherwise noted)

You will find through these variables that security for these pipelines are awful, and you are more than welcome to try and improve it.

## HOST
The remote host for where to send the files to.

## PASSWORD
The password for a user that has access to the folder(s) that the twitter bot is getting deployed to (since SFTP uploading is used to get the files up to the remote server).

## USERNAME
The username of the remote user that has access to the folder(s) that the twitter bot is getting deployed to.

## SERVER_PATH
The path of where to deploy the prod twitter bot files to, with a trailing right slash at the end.

## SERVER_PORT
The SSH port of the remote server.

## SSH_PRIVATE_KEY
A SSH private key that can be used to login to the remote server. Secure, right!
