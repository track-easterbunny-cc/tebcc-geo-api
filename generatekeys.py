#   track.easterbunny.cc Geo API v1.3.0
#   Copyright (C) 2020  track.easterbunny.cc
#
#   The track.easterbunny.cc Geo API is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   The track.easterbunny.cc Geo API is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with the track.easterbunny.cc Geo API.  If not, see <http://www.gnu.org/licenses/>.

try:
    from secrets import token_hex
    secrets_avail = True
except ImportError:
    import uuid
    secrets_avail = False
    print("Unable to use secrets library to generate keys, defaulting on UUID library. Please upgrade to Python 3.6 "
          "for more securely generated keys.")

import sys

print("Welcome to the API key generator for the Geo API. This script generates 25-byte hex keys for use with the "
      "Geo API.")
print("How many keys would you like to generate (up to 32)?")
number = input("Input here: ")

try:
    number = int(number)
except ValueError:
    print("You didn't input a valid integer. Please try again.")
    sys.exit()

if number <= 0 or number > 32:
    print("Please choose a number of keys to generate that is greater than 0, and less than or equal to 32.")
    sys.exit()

keys = []
for i in range(1, number + 1):
    if secrets_avail is True:
        keys.append(token_hex(25))
    else:
        key = uuid.uuid4().hex + uuid.uuid4().hex
        key = key[:50]

keys_raw = keys
keys = str(keys)
keys = keys.replace("[", "").replace("]", "")
print("Done! Copy & paste the line below, and replace the line of code that has the api_key variable.")
print("")
print("api_key = [" + keys + "]")
print("")
print("If you'd rather append these keys to your current list of API keys, delete the ] symbol at the end of the")
print("api_key variable line of code, and then paste this in at the end of the line of code.")
print("")
print(", " + keys + "]")
sys.exit()
