from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base, sessionmaker, Session
from dotenv import load_dotenv
import os
load_dotenv()

engine = create_engine(os.getenv("DATABASE_URL"))
SessionLocal = sessionmaker(bind=engine)
Base = declarative_base()


def get_session() -> Session:
    session = SessionLocal()
    try:
        return session
    except Exception as e:
        session.close()
        raise e
