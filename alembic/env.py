from logging.config import fileConfig

from sqlalchemy import engine_from_config
from sqlalchemy import pool

from alembic import context

# Import your models
from ua_extended_data.model import UAExtendedDataORM
from user_location.model import UserLocationORM
from repository import Base

# This is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)

# Add your model's MetaData object here
# for 'autogenerate' support.
target_metadata = Base.metadata

# Other settings from the config, defined in alembic.ini
# are available here as well.
url = config.get_main_option("sqlalchemy.url")
engine = engine_from_config(
    {"sqlalchemy.url": url},
    prefix="sqlalchemy.",
    poolclass=pool.NullPool,
)

# connect to the database using the URL
with engine.connect() as connection:
    context.configure(
        connection=connection,
        target_metadata=target_metadata,
        compare_type=True,
        compare_server_default=True,
        compare_ignore_autoincrement=True,
    )

    # this will generate alembic/env.py file
    with context.begin_transaction():
        context.run_migrations()
