"""Set timestamp fields to be indexes

Revision ID: b7233475baaa
Revises: da783ae25736
Create Date: 2024-04-01 14:06:25.528284

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = 'b7233475baaa'
down_revision: Union[str, None] = 'da783ae25736'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_index(op.f('ix_ua_extended_data_client_request_ts'), 'ua_extended_data', ['client_request_ts'], unique=False)
    op.create_index(op.f('ix_ua_extended_data_server_request_ts'), 'ua_extended_data', ['server_request_ts'], unique=False)
    op.create_index(op.f('ix_user_location_timestamp'), 'user_location', ['timestamp'], unique=False)
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_user_location_timestamp'), table_name='user_location')
    op.drop_index(op.f('ix_ua_extended_data_server_request_ts'), table_name='ua_extended_data')
    op.drop_index(op.f('ix_ua_extended_data_client_request_ts'), table_name='ua_extended_data')
    # ### end Alembic commands ###
