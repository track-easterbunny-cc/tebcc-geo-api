# Config documentation
This document outlines the entirety of the config.ini file. Additionally - there is one note about alembic.ini and configuring your connection string.

Please also see the `ENV.md` file to finalize proper database configuration.

# HITS section
Controls stuff regarding the hit counting functionalities of the Geo API.

## hitboundary_historicalhigh
The upper boundary for hits to be collected from a historical standpoint. After this timestamp, historical data collection is disabled.

Format: `UNIX timestamp as integer`. Defaults to `1711965660`, or 60 seconds after `hitboundary_high`.

## hitboundary_high
The upper boundary for hits to be counted overall. After this timestamp, any hits to the `/api/v1/ipLocation` endpoint aren't counted.

Format: `UNIX timestamp as integer`. Defaults to `1711965600` - for Easter 2024, this was April 1, 2024 at 6:00 AM EDT (Monday morning).

## hitboundary_tover
The boundary for hits during tracking. This is only for the insights page to determine where to cut off hits when selecting the box to only show hits during tracking.

Format: `UNIX timestamp as integer`. Defaults to `1711879860` - for Easter 2024, this was March 31, 2024 at 6:11 AM EDT.

## hitboundary_low
The lower boundary for hits to be counted overall. Before this timestamp, any hits to the `/api/v1/ipLocation` endpoint aren't counted.

Format: `UNIX timestamp as integer`. Defaults to `1711778340` - for Easter 2024, this was March 30, 2024 at 1:59 AM EDT.

## hitboundary_historicallow
The lower boundary for hits to be collected from a historical standpoint. Before this timestamp, historical data collection is disabled.

Format: `UNIX timestamp as integer`. Defaults to `1711778280` - for Easter 2024, this was March 30, 2024 at 1:58 PM EDT.

# HISTORICAL section
Controls stuff regarding the historical hit loading of the Geo API. Proper values are needed to reference the older hit data properly.

## 2022boundary_high
The high-end boundary of the 2022 historical hit data. 

Format: `UNIX timestamp as integer`. Defaults to `1650189720`, which is Sunday, April 17, 2022 at 6:02 AM EDT.

## 2022boundary_low
The low-end boundary of the 2022 historical hit data.

Format: `UNIX timestamp as integer`. Defaults to `1650088680`, which is Saturday, April 16, 2022 at 1:58 AM EDT.

## 2023boundary_high
The high-end boundary of the 2023 historical hit data. 

Format: `UNIX timestamp as integer`. Defaults to `1681120800`, which is Monday, April 10, 2023 at 6:00 AM EDT.

## 2023boundary_low
The low-end boundary of the 2023 historical hit data. 

Format: `UNIX timestamp as integer`. Defaults to `1680933480`, which is Saturday, April 8, 2023 at 1:58 AM EDT.

# MAPBOX section
This section controls Mapbox related settings.

## geocoder_key
A mapbox key that can access their reverse geocoder API. Should start with `pk.`

Format: `A string`

# GEOAPI_SETTINGS section
Controls a lot of the Geo API settings.

## cors_url
The URL that requests will be accepted from for the purpose of CORS.

Format: `URL as a string`

## api_key
The main API key for the `/api/v1/ipLocation` and `/api/v1/uaExtendedData` methods.

Format: `String`

## api_key_hitter
The API key for the `/api/v1/insights/logHistorical` method.

Format: `String`

## api_key_gethit
The API key for the `/api/v1/insights/historical` method.

Format: `String`

## api_key_postdate
The API key for the `/api/v1/postDBdate` method.

Format: `String`

## api_version
The version the API reports back as in requests.

Format: `String`

## api_version_nov
The version the API reports back as in requests, but without the v.

This could easily be replaced with just doing `.replace("v", "")` on the normal API version, but remember, this codebase is not good.

Format: `String`

## db_date
Ignore this.

Format: `Ignore this`

## geoip2_database_path
The fully qualified path to the `GeoLite2-City.mmdb` file that the Geo API runs from. Try to avoid tricks with relative paths on this config variable, using a full filesystem path is the best way to avoid any issues.

Format: `Filename string`

## *_url
Really anything that ends in `_url` is a URL that will display on the homepage of the Geo API to link to somewhere. All the options are very much self explanatory.

Format: `URL as a string`

## homepage_tracker_version
The version of the tracker to display on the homepage.

Format: `String`

# DB section
Configures a few sections with how the database works.

## db_date_path
The fully qualified path to the dbdate.txt file which contains the date of the latest Geo API database.

For best results, ensure this file is just `YYYYMMDD` of the latest Geo API database. Don't put a newline at the end, yada, you get the point.

Similar to the `geoip2_database_path` configuration option, this should be a fully qualified path, relative paths will probably cause pain.

Format: `filename string`

## db_post_key
Ignore, for it is not used anymore.

Format: `To be ignored`

# INSIGHTS section
Just a couple of settings to configure access to the insights page.

## username
The username to access the insights page.

Format: `String`

## password
The password to access the insights page.

Format: `String`
