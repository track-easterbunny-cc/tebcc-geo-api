#   track.easterbunny.cc Geo API v1.7.0
#   Copyright (C) 2024 track.easterbunny.cc
#
#   The track.easterbunny.cc Geo API is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   The track.easterbunny.cc Geo API is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with the track.easterbunny.cc Geo API.  If not, see <http://www.gnu.org/licenses/>.
#
#   Source code is available at https://gitlab.com/track-easterbunny-cc/geoapi
#   License is available in the LICENSE file, or at https://www.gnu.org/licenses/agpl-3.0-standalone.html

from flask import Flask, request, jsonify, render_template
from flask_httpauth import HTTPBasicAuth
from flask_cors import CORS
from werkzeug.security import generate_password_hash, check_password_hash
import geoip2.database
import threading
import json
import time
import traceback
from configparser import ConfigParser
import requests

from ua_extended_data.entity import UAExtendedDataWithID
from ua_extended_data.repository import UAExtendedDataRepository

from user_location.entity import UserLocationWithID
from user_location.repository import UserLocationRepository

cp = ConfigParser()
cp.read("config.ini")

# Configuration variables - read the Wiki on GitLab for more info
cors_url = cp.get("GEOAPI_SETTINGS", "cors_url")
api_key = cp.get("GEOAPI_SETTINGS", "api_key")
api_key_hitter = cp.get("GEOAPI_SETTINGS", "api_key_hitter")
api_key_gethit = cp.get("GEOAPI_SETTINGS", "api_key_gethit")
api_key_postdbdate = cp.get("GEOAPI_SETTINGS", "api_key_postdate")
api_version = cp.get("GEOAPI_SETTINGS", "api_version")
api_version_nov = cp.get("GEOAPI_SETTINGS", "api_version_nov")
with open("/var/www/tebccgeoapi/dbdate.txt", "r") as dbfile:
    db_date = dbfile.read()
geoip2_database_path = cp.get("GEOAPI_SETTINGS", "geoip2_database_path")
source_url = cp.get("GEOAPI_SETTINGS", "source_url")
geoapi_source_url = cp.get("GEOAPI_SETTINGS", "geoapi_source_url")
base_url = cp.get("GEOAPI_SETTINGS", "base_url")
homepage_tracker_url = cp.get("GEOAPI_SETTINGS", "homepage_tracker_url")
homepage_privacy_url = cp.get("GEOAPI_SETTINGS", "homepage_privacy_url")
homepage_faq_url = cp.get("GEOAPI_SETTINGS", "homepage_faq_url")
homepage_news_url = cp.get("GEOAPI_SETTINGS", "homepage_news_url")
homepage_acknowledgements_url = cp.get("GEOAPI_SETTINGS", "homepage_acknowledgements_url")
homepage_license_url = cp.get("GEOAPI_SETTINGS", "homepage_license_url")
homepage_knownissues_url = cp.get("GEOAPI_SETTINGS", "homepage_knownissues_url")
homepage_urlarguments_url = cp.get("GEOAPI_SETTINGS", "homepage_urlarguments_url")
homepage_iconassets_url = cp.get("GEOAPI_SETTINGS", "homepage_iconassets_url")
homepage_tracker_version = cp.get("GEOAPI_SETTINGS", "homepage_tracker_version")
mapbox_geocoder_key = cp.get("MAPBOX", "geocoder_key")

insights_username = cp.get("INSIGHTS", "username")
insights_password = cp.get("INSIGHTS", "password")

acceptable_gmaps_formats = [
    ["locality", "political"],
    ["administrative_area_level_1", "political"],
    ["country", "political"]
]

reader = geoip2.database.Reader(geoip2_database_path)

app = Flask(__name__)
cors = CORS(app)
auth = HTTPBasicAuth()

users = {
    insights_username: insights_password
}

time_boundary_low = cp.getint("HITS", "hitboundary_low")
time_boundary_high = cp.getint("HITS", "hitboundary_high")

time_boundary_historical_low = cp.getint("HITS", "hitboundary_historicallow")
time_boundary_historical_high = cp.getint("HITS", "hitboundary_historicalhigh")

historical_2022_delta = cp.getint("HITS", "hitboundary_historicallow") - cp.getint("HISTORICAL", "2022boundary_low")
historical_2023_delta = cp.getint("HITS", "hitboundary_historicallow") - cp.getint("HISTORICAL", "2023boundary_low")


@auth.verify_password
def verify_password(username, password):
    if username in users and \
            check_password_hash(users.get(username), password):
        return username


hit_lock = threading.Lock()
hitter_lock = threading.Lock()
extended_ua_lock = threading.Lock()


@app.route("/api/v1/uaExtendedData", methods=['POST'])
def ua_extended_data():
    key = request.values.get('key', "None")

    if key != api_key:
        response = {"apiversion": api_version, "code": 5, "data": {},
                    "message": "The API key is incorrect. Please try again."}
        return jsonify(response), 400

    try:
        ua_data_client = request.get_json()
    except json.JSONDecodeError:
        response = {"apiversion": api_version, "code": 1, "data": {},
                    "message": "JSON data failed to decode"}
        return jsonify(response), 400

    current_time = time.time()
    ua_model = UAExtendedDataWithID(
        architecture=ua_data_client.get('architecture', ''),
        bitness=ua_data_client.get('bitness', ''),
        client_request_ts=ua_data_client['client_request_ts'],
        server_request_ts=current_time,
        full_version_list=json.dumps(ua_data_client.get('fullVersionList', {})),
        mobile=ua_data_client.get('mobile', False),
        model=ua_data_client.get('model', ''),
        original_ua=ua_data_client.get('original_ua', ''),
        platform=ua_data_client.get('platform', ''),
        platform_version=ua_data_client.get('platformVersion', ''),
        ua_full_version=ua_data_client.get('uaFullVersion', ''),
        ip_remote_addr=request.environ['REMOTE_ADDR'],
        cf_connecting_ip=request.headers.get("CF-Connecting-IP")
    )

    data_logged = False

    if time_boundary_low <= current_time <= time_boundary_high or time_boundary_low <= ua_data_client['client_request_ts'] <= time_boundary_high:
        try:
            repository = UAExtendedDataRepository()
            repository.add_ua_data(ua_model)
            data_logged = True
        except:
            response = {"apiversion": api_version, "code": 7, "data": {},
                        "message": "Failed to upload UA Data (repository error)"}
            return jsonify(response), 400

    response = {"apiversion": api_version, "code": 0, "data": {"data_logged": data_logged},
                "message": "Data uploaded successfully."}
    return jsonify(response), 200


@app.route("/api/v1/ipLocation", methods=['GET'])
def iploc():
    key = request.values.get('key', "None")
    hit_count = request.values.get('hitcount', "None")

    if key != api_key:
        response = {"apiversion": api_version, "code": 5, "data": {},
                    "message": "The API key is incorrect. Please try again."}
        return jsonify(response), 400

    ip = request.headers.get('CF-Connecting-IP')
    if ip is None:
        ip = request.remote_addr

    # Add hit counter code here w/ blank except
    current_time = time.time()
    hit_counted_reason = ""

    if hit_count == "true" or "track.easterbunny.cc" in request.referrer:
        if time_boundary_low <= current_time <= time_boundary_high:
            try:
                with hit_lock:
                    with open("hit.json", "r") as file:
                        data = json.load(file)

                    data['hits'] = data['hits'] + 1

                    with open("hit.json", "w") as file:
                        json.dump(data, file)

                    hit_counted_reason = "Hit counted"
            except:
                hit_counted_reason = "Failed to lock the file"
                pass
        else:
            hit_counted_reason = "Time boundary out of bounds"
    else:
        hit_counted_reason = "Referrer invalid or hit count arg not specified"

    mm2response = reader.city(ip)

    mm2_lat = mm2response.location.latitude
    mm2_lng = mm2response.location.longitude
    mm2_acc = mm2response.location.accuracy_radius
    mm2_cityname = mm2response.city.name
    mm2_subdivison = mm2response.subdivisions.most_specific.name
    mm2_countryname = mm2response.country.name

    if mm2_cityname is None and mm2_subdivison is None and mm2_countryname is None:
        mm2_formattedname = f"{abs(mm2_lat)} {'N' if mm2_lat >= 0 else 'S'}, {abs(mm2_lng)} {'W' if mm2_lng < 0 else 'E'}"
    elif mm2_cityname is None and mm2_subdivison is None:
        mm2_formattedname = mm2_countryname
    elif mm2_subdivison is None:
        mm2_formattedname = mm2_cityname + ", " + mm2_countryname
    elif mm2_cityname is None:
        mm2_formattedname = mm2_subdivison + ", " + mm2_countryname
    else:
        mm2_formattedname = mm2_cityname + ", " + mm2_subdivison + ", " + mm2_countryname

    if mm2_lat is None:
        response = {"apiversion": api_version, "code": 7, "data": {},
                    "message": "No latitude/longitude could be found for the IP."}
        return jsonify(response), 400

    if time_boundary_low <= current_time <= time_boundary_high:
        try:
            repository = UserLocationRepository()
            user_loc = UserLocationWithID(
                latitude=mm2_lat,
                longitude=mm2_lng,
                accuracy=int(mm2_acc)
            )
            repository.add_user_location(user_loc)
        except:
            print("!! Failed to add a user location to the table. !!")


    response = {"apiversion": api_version, "db-date": db_date, "code": 0,
                "data": {"lat": mm2_lat, "lng": mm2_lng, "accuracy": mm2_acc, "rawip": ip,
                         "humanlocation": mm2_formattedname},
                "message": "The request was successful.",
                "hit_counted": hit_counted_reason,
                "attribution": "This product includes GeoLite2 data created by MaxMind, available from https://maxmind.com"}
    return jsonify(response), 200


@app.route("/api/v1/reverseGeoCode", methods=['GET', 'POST'])
def getReverseGeocode():
    key = request.values.get("key", "none")
    if key != "d26f9249c8a70860c73a9aaf019ec04124ce92e2ee5a4b37f2":
        response = {"apiversion": api_version, "code": 0, "data": {},
                    "message": "Invalid API key."}
        return jsonify(response), 400

    lat = request.values.get("lat", "none")
    lng = request.values.get("lng", "none")

    if lat == "none" or lng == "none":
        response = {"apiversion": api_version, "code": 1, "data": {},
                    "message": "Invalid lat/lng."}
        return jsonify(response), 400

    lat = float(lat)
    lng = float(lng)

    current_time = time.time()
    hit_counted_reason = ""

    if time_boundary_low <= current_time <= time_boundary_high:
        try:
            with hit_lock:
                with open("hit.json", "r") as file:
                    data = json.load(file)

                data['geocodes'] = data['geocodes'] + 1

                with open("hit.json", "w") as file:
                    json.dump(data, file)
        except:
            hit_counted_reason = "Failed to lock the file"
            pass
    else:
        hit_counted_reason = "Time boundary out of bounds"

    r = requests.get(f"https://api.mapbox.com/geocoding/v5/mapbox.places/{lng},{lat}.json?types=place%2Cregion%2Ccountry%2Clocality&limit=1&access_token={mapbox_geocoder_key}")
    if r.status_code != 200:
        response = {"error": "Mapbox request failed", "hit_counted": hit_counted_reason}
        return jsonify(response), 400

    r = r.json()
    try:
        placename = r['features'][0]['place_name']
        response = {"apiversion": api_version, "code": 2, "data": {"location": placename},
                    "message": "Location returned successfully.", "hit_counted": hit_counted_reason}
        return jsonify(response), 200
    except KeyError:
        response = {"apiversion": api_version, "code": 3, "data":
            {"location": f"{round(lat, 4)}°{'N' if lat >= 0 else 'S'}, {round(lng, 4)}°{'W' if lng < 0 else 'E'}"},
            "message": "Location returned successfully.", "hit_counted": hit_counted_reason}
        return jsonify(response), 400


@app.route("/api/v1/getDBdate", methods=['GET'])
def getDBdate():
    with open("/var/www/tebccgeoapi/dbdate.txt", "r") as dbfile:
        return dbfile.read()


@app.route("/api/v1/postDBdate", methods=['POST'])
def postDBdate():
    key = request.values.get("key", "none")
    db_date = request.values.get("dbdate", "none")
    if key != api_key_postdbdate:
        return jsonify({"Error": "API key invalid"})

    with open("/var/www/tebccgeoapi/dbdate.txt", "w") as dbfile:
        dbfile.write(db_date)

    return jsonify({"Success": "DB date has been modified."})


@app.route("/", methods=['GET'])
def defaultresponse():
    with open("/var/www/tebccgeoapi/dbdate.txt", "r") as dbfile:
        dbdate = dbfile.read()

    return render_template('homepage.html', apiver=api_version_nov, geodbver=dbdate, privacy_url=homepage_privacy_url,
                           tracker_url=homepage_tracker_url, source_url=source_url,
                           tracker_ver=homepage_tracker_version, geoapi_source_url=geoapi_source_url,
                           geoapi_license_url=base_url + "/license/")


@app.route("/license/", methods=['GET'])
def license():
    return render_template('license.html', geoapi_source_url=geoapi_source_url, base_url=base_url)


@app.route("/insights/", methods=['GET'])
@auth.login_required
def hits():
    try:
        with hit_lock:
            with open("hit.json", "r") as file:
                data = json.load(file)

            hit_count = data['hits']
            hit_count_geocoder = data['geocodes']
    except:
        return "Error reading file."

    try:
        with hitter_lock:
            with open("historicalhit.json", "r") as file:
                data2 = json.load(file)
    except:
        return "Error reading historical hits."

    return render_template("hits.html", hitnumber='{:,}'.format(hit_count), hitnumber_raw=hit_count,
                           historicaldata=data2, sixam_cutoff=cp.getint("HITS", "hitboundary_tover"),
                           hit_boundary_high=cp.getint("HITS", "hitboundary_historicalhigh"),
                           hitnumber_geocoder='{:,}'.format(hit_count_geocoder))


@app.route("/api/v1/insights/logHistorical", methods=['POST'])
def historicalhitter():
    key = request.values.get('key', "None")

    if key != api_key_hitter:
        return "Bad api key"

    # Code for locking by time goes here.
    current_time = time.time()

    if not time_boundary_historical_low <= current_time <= time_boundary_historical_high:
        return "Data not logged - right now, historical data can't be logged."

    try:
        with hit_lock:
            with open("hit.json", "r") as file:
                data = json.load(file)
    except:
        return "Error reading file."

    with open("historicalhit_2022.json", "r") as file:
        data2022 = json.load(file)

    with open("historicalhit_2023.json", "r") as file:
        data2023 = json.load(file)

    hits = data['hits']
    timestamp = str(int(time.time() // 60 * 60))

    try:
        with hitter_lock:
            with open("historicalhit.json", "r") as file:
                data2 = json.load(file)

            data2['historical'][timestamp] = {}
            data2['historical'][timestamp]['hits'] = hits
            try:
                data2['historical'][timestamp]['2022hits'] = \
                data2022['historical'][str(int(timestamp) - historical_2022_delta)]['hits']
            except KeyError:
                pass

            try:
                data2['historical'][timestamp]['2023hits'] = \
                data2023['historical'][str(int(timestamp) - historical_2023_delta)]['hits']
            except KeyError:
                pass

            if hits > 128571:
                gmapscost = round(700 + ((hits - 128571) * 0.0056), 2)
            elif hits >= 28571:
                gmapscost = round((hits - 28571) * 0.007, 2)
            else:
                gmapscost = 0
                
            data2['historical'][timestamp]['gmapscost'] = gmapscost
            try:
                data2['historical'][timestamp]['2022gmapscost'] = \
                data2022['historical'][str(int(timestamp) - historical_2022_delta)]['gmapscost']
            except KeyError:
                pass

            try:
                data2['historical'][timestamp]['2023gmapscost'] = \
                data2023['historical'][str(int(timestamp) - historical_2023_delta)]['gmapscost']
            except KeyError:
                pass

            with open("historicalhit.json", "w") as file:
                json.dump(data2, file)
    except:
        print(traceback.format_exc())
        return "Error writing to file."

    return "OK!"


# These two methods do exactly the same thing. Why? idk
@app.route("/api/v1/insights/historical", methods=['GET'])
def historicalfile():
    key = request.values.get('key', "None")

    if key != api_key_gethit:
        return "Bad api key"

    try:
        with hitter_lock:
            with open("historicalhit.json", "r") as file:
                data = json.load(file)
    except:
        return "Failed to load data."

    return jsonify(data)


@app.route("/api/v1/insights/fetchHistoricalData", methods=['GET'])
def fetchdata():
    key = request.values.get('key', "None")

    if key != api_key_gethit:
        return "Bad api key"

    try:
        with hitter_lock:
            with open("historicalhit.json", "r") as file:
                data = json.load(file)
    except:
        return "Failed to load historical data."

    returneddata = {"historicaldata": data}

    return jsonify(returneddata)


@app.route("/api/v1/insights/fetchHitCount", methods=['GET'])
def fetchHitCount():
    key = request.values.get('key', "None")

    if key != api_key_gethit:
        return "Bad api key"

    try:
        with hit_lock:
            with open("hit.json", "r") as file:
                data2 = json.load(file)
    except:
        return "Failed to load hit counter."

    returneddata = {"hits": data2['hits'], "hits_formatted": '{:,}'.format(data2['hits']),
                    "hits_geocoder": data2['geocodes'], "hits_geocoder_formatted": '{:,}'.format(data2['geocodes'])}

    return jsonify(returneddata)


@app.route("/api/v1/getVisualizationData", methods=['GET'])
def visualizationData():
    key = request.values.get("key", "none")

    if key != api_key_gethit:
        return "Bad api key"

    repository = UserLocationRepository()

    return jsonify(repository.get_user_location_data())


if __name__ == "__main__":
    app.run(threaded=True)

