from sqlalchemy import Column, Integer, DECIMAL, VARCHAR, BOOLEAN

from repository import Base


class UAExtendedDataORM(Base):
    __tablename__ = "ua_extended_data"

    id: int = Column(Integer, primary_key=True, index=True, nullable=False)
    architecture: str = Column(VARCHAR(16), nullable=False)
    bitness: str = Column(VARCHAR(8), nullable=False)
    client_request_ts: float = Column(DECIMAL(64, 3, asdecimal=False), nullable=False, index=True)
    server_request_ts: float = Column(DECIMAL(64, 3, asdecimal=False), nullable=False, index=True)
    full_version_list: str = Column(VARCHAR(4096), nullable=False)
    mobile: bool = Column(BOOLEAN, nullable=False)
    model: str = Column(VARCHAR(256), nullable=False)
    original_ua: str = Column(VARCHAR(1024), nullable=False)
    platform: str = Column(VARCHAR(128), nullable=False)
    platform_version: str = Column(VARCHAR(128), nullable=False)
    ua_full_version: str = Column(VARCHAR(128), nullable=False)
    ip_remote_addr: str = Column(VARCHAR(128), nullable=False)
    cf_connecting_ip: str = Column(VARCHAR(128), nullable=True)
