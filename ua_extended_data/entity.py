from pydantic import BaseModel
from typing import Optional


class UAExtendedData(BaseModel):
    """UA Extended Data object."""
    architecture: str
    bitness: str
    client_request_ts: float
    server_request_ts: float
    # Full Version List is JSON-stringified.
    full_version_list: str
    mobile: bool
    model: str
    original_ua: str
    platform: str
    platform_version: str
    ua_full_version: str
    ip_remote_addr: str
    cf_connecting_ip: Optional[str] = None


class UAExtendedDataWithID(UAExtendedData):
    """UA Extended Data object with an ID"""
    id: Optional[int] = None
