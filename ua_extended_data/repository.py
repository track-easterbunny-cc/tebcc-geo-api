from repository.base_repository import BaseRepository
from repository import get_session

from ua_extended_data.model import UAExtendedDataORM
from ua_extended_data.entity import UAExtendedDataWithID


class UAExtendedDataRepository(BaseRepository):
    def __init__(self):
        super().__init__(session=get_session())

    def add_ua_data(self, ua_data: UAExtendedDataWithID):
        new_ua_data = UAExtendedDataORM(**ua_data.model_dump())

        self.session.add(new_ua_data)
        self.session.commit()
