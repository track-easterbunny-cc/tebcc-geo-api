# TEBCC Geo API
The TEBCC Geo API is a complementary API to track.easterbunny.cc that does...a lot of things. Originally to just figure out what your IP corresponded to as an approximate location, it is now in charge of the following functions:
* Calculating an approximate location from an IP address, powering the Easter Bunny estimated arrival time feature in track.easterbunny.cc
* Answering precise location reverse geocodes for the tracker (so the user knows the tracker found their location correctly)
* Logging data from UA Client Hints capable browsers that no longer send this data as part of the user agent (since the Geo API assumed the role of being some sort of analytics platform for the tracker)
* Storing results for all approximate location calls so that I could make a fancy heatmap for the final year of tracking
* Also hosting the infamous insights page that can see into how the tracker is performing during tracking.

It basically grew way outside of the scope that was necessary and is held together with string and gum. My apologies, this codebase is a nightmare to work with.

# Requirements/Setup
To get the Geo API going, you'll need a couple of accounts:
* An account with MaxMind to get their GeoLite2 database
* An account with Mapbox for their reverse geocoder

You'll also need a Python 3 environment with these libraries:
* Flask
* Flask_HTTPAuth
* GeoIP2
* Requests
* Alembic
* Pydantic
* SQLAlchemy

Latest version of these libraries should work a charm. No requirements.txt file here unfortunately.

You'll also need some database to work with (that SQLAlchemy/Alembic can work with). In this case, you have a few options:
* Use MariaDB, and you can use everything as is. This was the setup used in production.
* Use SQLite3 (local file), and you should be able to use everything as is. Just change the database URL to `sqlite://yourdbfile.db`
* Use a different DBMS, and you will need to regenerate the Alembic files to set up the database. Do this by clearing the alembic/versions folder, then `alembic revision --autogenerate -m "Initial revision"`.

From MaxMind, make sure you download the latest GeoLite2 City file - you'll want the .mmdb file version. Usually they're compressed as a .tar.gz, but extract this file and you'll get the .mmdb file.

Regardless of your DBMS, you will need to run `alembic upgrade head` to get everything set up.

You'll want to peek at both CONFIG.md and ENV.md to figure out how to configure your config & environment variables - including the database connector schema.

## Don't want to deal with the DBMS?
Congrats! I can't blame you. In that case, these are the modifications to the code you'll need to make:
* Remove all imports regarding `UAExtendedDataWithID`, `UAExtendedDataRepository`, `UserLocationWithID`, and `UserLocationRepository`.
* Then gutter out all the code that deals with writing to the repositories.

If you use an IDE like PyCharm, it should help you get out all the errors when you delete the imports to these files.

# Config setup
There's a few things you'll need to setup in the config file. Namely, the API keys used for the Geo API endpoints, the path to your GeoLite2 database file, and the mapbox geocoder key. Those are pretty cleanly labelled in the config file, just throw in values that work for you.

# Getting the server running

You'll then need some way to serve the server, you can either do that by just running `python main.py` and use the built in werkzeug server on port 5000. Alternatively you can look at a WSGI module for your preferred web server - apache or nginx has good guides on getting this running.

Running the Geo API on a dedicated web server (that is also shared with TEBCC) is the best idea because the CI pipelines in the main project also handle updating the GeoLite2 database, assuming you have a normal Linux box where the Geo API has a static location. You can try serverless, but the auto-update CI part of the pipeline may not work so well.

# The insights page
The Geo API insights page is quite the beast and I'm going to leave it mostly undocumented here.

There's a few things to note if you do want to use it. First - make sure you have a hits.json file with just `{"hits": 0, "geocodes": 0}` in it. There's a reference to `historicalhit_2022.json` and `historicalhit_2023.json`, which is just the historical hits from 2022 and 2023 respectively. I'll leave this file in this repo so things don't break. 

The system that tracks historical hits isn't very sustainable long-term since everything gets stuffed into one JSON file that has fields for 2022/2023/2024 hits as of this year. It should definitely be each year has a file and it's read individually.

Additionally, you'll want `historicalhit.json` to start with `{}`.

To get these historical things to work, you'll want a cronjob to hit `/api/v1/insights/logHistorical` every minute.

There's also a username/pass to configure for the frontend to make that work too.

Hopefully the insights thing isn't too confusing. 