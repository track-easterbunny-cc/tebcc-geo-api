from repository.base_repository import BaseRepository
from repository import get_session

from user_location.model import UserLocationORM
from user_location.entity import UserLocationWithID

from sqlalchemy import func


class UserLocationRepository(BaseRepository):
    def __init__(self):
        super().__init__(session=get_session())

    def add_user_location(self, user_location: UserLocationWithID):
        new_user_location = UserLocationORM(**user_location.model_dump())

        self.session.add(new_user_location)
        self.session.commit()

    def get_user_location_with_id(self, record_id: int):
        ul = self.session.query(UserLocationORM).filter_by(id=record_id).one_or_none()

        return UserLocationWithID(**ul.__dict__)

    def set_user_location_with_id(self, user_location: UserLocationWithID):
        existing_ul = self.session.query(UserLocationORM).filter_by(id=user_location.id).one_or_none()

        ul_dict = user_location.model_dump()

        for key, value in ul_dict.items():
            setattr(existing_ul, key, value)

        self.session.commit()

    def get_user_location_data(self):
        query = self.session.query(UserLocationORM.latitude, UserLocationORM.longitude,
                                   func.count().label('count')).group_by(UserLocationORM.latitude,
                                                                         UserLocationORM.longitude).all()

        return [{"lat": row.latitude, "lng": row.longitude, "cnt": row.count} for row in query]
