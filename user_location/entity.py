from pydantic import BaseModel
from typing import Optional

class UserLocation(BaseModel):
    """User Location model from a hit."""
    latitude: float
    longitude: float
    accuracy: int
    timestamp: int


class UserLocationWithID(UserLocation):
    """User Location object with an ID"""
    id: Optional[int] = None
