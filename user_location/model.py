from sqlalchemy import Column, Integer, DECIMAL

from repository import Base

class UserLocationORM(Base):
    __tablename__ = "user_location"

    id: int = Column(Integer, primary_key=True, index=True, nullable=False)
    latitude: float = Column(DECIMAL(8, 5, asdecimal=False), nullable=False)
    longitude: float = Column(DECIMAL(8, 5, asdecimal=False), nullable=False)
    accuracy: int = Column(Integer, nullable=False)
    timestamp: int = Column(Integer, index=True, nullable=False)
